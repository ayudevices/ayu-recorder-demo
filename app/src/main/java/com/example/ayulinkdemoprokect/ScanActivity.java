package com.example.ayulinkdemoprokect;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.ayu.recorder.AyuDeviceScanner;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class ScanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_scan);

        setupViews();
    }

    private void setupViews(){
        ProgressBar progressBar = findViewById(R.id.searching_device);

        findViewById(R.id.start_search).setOnClickListener((view)->{
            if(progressBar.getVisibility() == View.GONE){
                progressBar.setVisibility(View.VISIBLE);

                AyuDeviceScanner.fillOutDevices(ScanActivity.this, usbDevice -> {

                    Dialog dialog = new MaterialAlertDialogBuilder(ScanActivity.this)
                            .setView(R.layout.device_found_dialogue)
                            .create();

                    ScanActivity.this.runOnUiThread(dialog::show);

                    dialog.findViewById(R.id.btn_on_device_found).setOnClickListener((view1)->{
                        final Intent intent = new Intent(ScanActivity.this, GraphActivity.class);

                        ScanActivity.this.runOnUiThread(()->{
                            startActivity(intent);
                        });
                    });
                });
            }
        });
    }
}
