package com.example.ayulinkdemoprokect;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ayu.recorder.AudioPermissionHelper;
import com.ayu.recorder.AudioRecorder;
import com.ayu.recorder.AudioRecorderInterface;
import com.ayu.recorder.PlayBack;
import com.ayu.recorder.WaveSaver;
import com.ayu.visualiser.AyuVisualizerView;

import java.util.Arrays;

public class GraphActivity extends AppCompatActivity implements AudioRecorderInterface.OnSampleReceivedListener {

    private static final String TAG = GraphActivity.class.getCanonicalName();
    private static final long Y_MAX = 10000, Y_MIN = -10000, X_MAX = 100, X_MIN = 0;
    private static final int STEP = 80;

    private AyuVisualizerView graph;

    private PlayBack playBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        graph = findViewById(R.id.graph);
        initGraph();

        playBack = PlayBack.create();

        if(AudioPermissionHelper.askPermissionForRecording(this)){
            AudioRecorder recorder = AudioRecorder.getInstance(this);
            recorder.addOnSampleReceivedListener(this);
            recorder.startRecording();
        }
    }

    private void initGraph(){
        graph.setMaxDataPoints(2000);
        graph.setMaxX(X_MAX);
        graph.setMinX(X_MIN);
        graph.setMaxY(Y_MAX);
        graph.setMinY(Y_MIN);
    }

    @Override
    public void onPause(){
        super.onPause();
        playBack.pause();
    }

    @Override
    public void onResume(){
        super.onResume();
        playBack.resume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        playBack.done();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSampleReceived(byte[] sample) {
        playBack.play(sample);
//        Log.i(TAG, Arrays.toString(sample));

        for(int i = 0; i < sample.length; i = i + STEP){

            try{
                plot(Arrays.copyOfRange(sample, i, i + STEP));
            } catch (ArrayIndexOutOfBoundsException e){
                // Do nothing
            }
        }
    }

    public void plot(byte[] data){
        runOnUiThread(()->{
            for(double amp : WaveSaver.getMinMax(data)){
                graph.addPoint(new AyuVisualizerView.DataPoint(0.1f, amp));
            }
        });
    }
}
